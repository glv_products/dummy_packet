unit DumyFoo;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

function SayHello: WideString;

implementation

function SayHello: WideString;
begin
  Result := 'Hello';
end;

end.
